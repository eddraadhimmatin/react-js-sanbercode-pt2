import React, { Component } from "react";

class Jam extends Component {
  constructor() {
    super();
    this.state = { time: new Date() };
  }

  componentDidMount() {
    this.update = setInterval(() => {
      this.setState({ time: new Date() });
    }, 1 * 1000);
  }

  componentWillUnmount() {
    clearInterval(this.update);
  }

  render() {
    const { time } = this.state;

    return (
      <div>
        <h2>Sekarang jam {time.toLocaleTimeString()}</h2>
      </div>
    );
  }
}

class JamCountdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: 100,
      visibile: true,
    };
  }

  componentDidMount() {
    if (this.props.start !== undefined) {
      this.setState({ time: this.props.start });
    }
    this.timerID = setInterval(() => this.tick(), 1000);
  }

  componentDidUpdate() {
    if (this.state.visibile) {
      if (this.state.time === 0) {
        clearInterval(this.timerID);
        this.setState({ visibile: false });
      }
    }
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      time: this.state.time - 1,
    });
  }

  render() {
    const { visibile } = this.state;
    return (
      <>
        {visibile === true ? (
          <div>
            <Jam />
            <h1>{this.state.time}</h1>
          </div>
        ) : (
          ""
        )}
      </>
    );
  }
}

export default JamCountdown;
