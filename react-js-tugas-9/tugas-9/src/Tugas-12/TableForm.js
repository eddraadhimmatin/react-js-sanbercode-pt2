import React from "react";
import MaterialTable from "material-table";

export default function TableForm() {
  const [state, setState] = React.useState({
    columns: [
      { title: "Nama Buah", field: "nama" },
      { title: "Harga (Rp)", field: "harga", type: "numeric" },
      { title: "Berat (kg)", field: "berat", type: "numeric" },
    ],
    data: [
      { nama: "Semangka", harga: 10000, berat: 1 },
      { nama: "Anggur", harga: 40000, berat: 0.5 },
      { nama: "Strawberry", harga: 30000, berat: 0.4 },
      { nama: "Jeruk", harga: 30000, berat: 1 },
      { nama: "Mangga", harga: 30000, berat: 0.5 },
    ],
  });

  return (
    <MaterialTable
      title="Table Buah"
      columns={state.columns}
      data={state.data}
      editable={{
        onRowAdd: (newData) =>
          new Promise((resolve) => {
            setTimeout(() => {
              resolve();
              setState((prevState) => {
                const data = [...prevState.data];
                data.push(newData);
                return { ...prevState, data };
              });
            }, 600);
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise((resolve) => {
            setTimeout(() => {
              resolve();
              if (oldData) {
                setState((prevState) => {
                  const data = [...prevState.data];
                  data[data.indexOf(oldData)] = newData;
                  return { ...prevState, data };
                });
              }
            }, 600);
          }),
        onRowDelete: (oldData) =>
          new Promise((resolve) => {
            setTimeout(() => {
              resolve();
              setState((prevState) => {
                const data = [...prevState.data];
                data.splice(data.indexOf(oldData), 1);
                return { ...prevState, data };
              });
            }, 600);
          }),
      }}
    />
  );
}
