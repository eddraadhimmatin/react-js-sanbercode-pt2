import React, { Component } from "react";
import FormBuah from "./Tugas-9/FormBuah";
import DaftarBuah from "./Tugas-10/DaftarBuah";
import JamCountdown from "./Tugas-11/JamCountdown";
import TableFrom from "./Tugas-12/TableForm";
import DaftarBuahHook from "./Tugas-13/DaftarBuahHook";

class App extends Component {
  render() {
    return (
      <div>
        {/* <FormBuah />
        <DaftarBuah />
        <JamCountdown />
        <TableForm /> */}
        <DaftarBuahHook />
      </div>
    );
  }
}

export default App;
