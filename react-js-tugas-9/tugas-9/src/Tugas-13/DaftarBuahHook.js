import React, { useState, useEffect } from "react";
import axios from "axios";
import "./DaftarBuahHook.css";

const DaftarBuahHook = () => {
  const [daftarBuah, setdaftarBuah] = useState(
    { nama: "Semangka", harga: 10000, berat: 1000 },
    { nama: "Anggur", harga: 40000, berat: 500 },
    { nama: "Strawberry", harga: 30000, berat: 400 },
    { nama: "Jeruk", harga: 30000, berat: 1000 },
    { nama: "Mangga", harga: 30000, berat: 500 }
  );

  const [inputName, setinputName] = useState("");
  const [inputHarga, setinputHarga] = useState("");
  const [inputBerat, setinputBerat] = useState(0);
  const [indexOfForm, setIndexOfForm] = useState(-1);

  const handleDelete = (event) => {
    let index = event.target.value;
    let newDaftarBuah = daftarBuah;
    let editedDataBuah = newDaftarBuah[indexOfForm];
    newDaftarBuah.splice(index, 1);

    if (editedDataBuah !== undefined) {
      var newIndex = newDaftarBuah.findIndex((el) => el === editedDataBuah);
      setdaftarBuah([...newDaftarBuah]);
      setIndexOfForm(newIndex);
    } else {
      setdaftarBuah([...newDaftarBuah]);
    }
    axios
      .delete("http://backendexample.sanbercloud.com/api/fruits/{ID_FRUIT}", {
        daftarBuah,
      })
      .then((res) => {});
  };

  const handleEdit = (event) => {
    let index = event.target.value;
    let namaBuah = daftarBuah.nama;
    let hargaBuah = daftarBuah.harga;
    let beratBuah = daftarBuah.berat;

    setinputName(namaBuah);
    setinputHarga(hargaBuah);
    setinputBerat(beratBuah);
    setIndexOfForm(index);
  };

  const handleChange = (event) => {
    let typeOfInput = event.target.name;
    switch (typeOfInput) {
      case "name": {
        setinputName(event.target.value);
        break;
      }
      case "harga": {
        setinputHarga(event.target.value);
        break;
      }
      case "berat": {
        setinputBerat(event.target.value);
        break;
      }
      default: {
        break;
      }
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    let nama = inputName;
    let harga = inputHarga.toString();
    let berat = inputBerat;

    if (nama.replace(/\s/g, "") !== "" && harga.replace(/\s/g, "") !== "") {
      let newDaftarBuah = setdaftarBuah;
      let index = setIndexOfForm;

      if (index === -1) {
        newDaftarBuah = [...newDaftarBuah, { nama, harga, berat }];
      } else {
        newDaftarBuah[index] = { nama, harga, berat };
      }
      setdaftarBuah(newDaftarBuah);
      setinputName("");
      setinputHarga("");
      setinputBerat(0);
    }

    axios
      .post("http://backendexample.sanbercloud.com/api/fruits", { daftarBuah })
      .then((res) => {});
  };

  useEffecct(() => {
    axios
      .get("http://backendexample.sanbercloud.com/api/fruits")
      .then((res) => {});
  });

  return (
    <>
      <h1>Daftar Harga Buah</h1>
      <table>
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          {daftarBuah.map((item, index) => {
            return (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>{item.nama}</td>
                <td>{item.harga}</td>
                <td>{item.berat / 1000} kg</td>
                <td>
                  <button onClick={handleEdit} value={index}>
                    Edit
                  </button>
                  &nbsp;
                  <button onClick={handleDelete} value={index}>
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <h1>Form Daftar Harga Buah</h1>
      <div style={{ width: "50%", margin: "0 auto", display: "block" }}>
        <div style={{ border: "1px solid #aaa", padding: "20px" }}>
          <form onSubmit={handleSubmit}>
            <label style={{ float: "left" }}>Nama:</label>
            <input
              style={{ float: "right" }}
              type="text"
              name="name"
              value={inputName}
              onChange={handleChange}
            />
            <br />
            <br />
            <label style={{ float: "left" }}>Harga:</label>
            <input
              style={{ float: "right" }}
              type="text"
              name="harga"
              value={inputHarga}
              onChange={handleChange}
            />
            <br />
            <br />
            <label style={{ float: "left" }}>Berat (dalam gram):</label>
            <input
              style={{ float: "right" }}
              type="number"
              name="berat"
              value={inputBerat}
              onChange={handleChange}
            />
            <br />
            <br />
            <div style={{ width: "100%", paddingBottom: "20px" }}>
              <button style={{ float: "right" }}>submit</button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
};

export default DaftarBuahHook;
