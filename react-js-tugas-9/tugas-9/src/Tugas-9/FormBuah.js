import React, { Component } from "react";
import "./style.css";

class FormBuah extends Component {
  render() {
    return (
      <div className="FormBuah">
        <div className="form">
          <h1>Form Pembelian Buah</h1>
          <div className="a1">
            <div className="a2">Nama Pelanggan</div>
            <input className="a3" type="text" name="name"></input>
          </div>
          <div className="a1">
            <div className="a2">Daftar Item</div>
          </div>

          <div className="a3">
            <input type="checkbox" name="semangka" value="semangka"></input>
            <label>Semangka</label>
            <br></br>
            <input type="checkbox" name="jeruk" value="jeruk"></input>
            <label>Jeruk</label>
            <br></br>
            <input type="checkbox" name="nanas" value="nanas"></input>
            <label>Nanas</label>
            <br></br>
            <input type="checkbox" name="salak" value="salak"></input>
            <label>Salak</label>
            <br></br>
            <input type="checkbox" name="anggur" value="anggur"></input>
            <label>Anggur</label>
            <br></br>
          </div>
          <div className="b1">
            <button>
              <a href="#" className="b2">
                Kirim
              </a>
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default FormBuah;
